const header = document.querySelector(".header");
const burgerButton = document.querySelector(".menu-icon");
const navicon = document.querySelector(".navicon");
const menu = document.querySelector(".menu");

burgerButton.addEventListener("click", () => {
    header.classList.toggle("header--opened")
    menu.classList.toggle("menu--opened")
    navicon.classList.toggle("navicon--active")
});