const gulp = require("gulp");
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require("gulp-autoprefixer");
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const minify = require("gulp-js-minify");
const concat = require("gulp-concat");
const imagemin = require("gulp-imagemin");

gulp.task("styles", function () {
    return gulp.src("src/scss/**/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(gulp.dest("dist/css"))
        .on("end", browserSync.reload);
});

gulp.task('scripts', function () {
    return gulp.src('src/js/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
});

gulp.task("img", function () {
    return gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'));
    })
    
gulp.task("serve", function () {
    browserSync.init({
        server: "./",
        injectChanges: true
    });

    gulp.watch("src/scss/**/*.scss", gulp.series("styles"));
    gulp.watch("src/js/**/*.js", gulp.series("scripts"));
    gulp.watch("./*.html").on("change", browserSync.reload);
});